clear
clc
close all

default

BPR = 0:0.1:14;

I = [];

for bpr = BPR
    [i, tsfc, ~, ~, ~] = perfomance_turbofan(Z, M, Tmax, bpr, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);

    I = [I i];
end

figure("Name", "I")
plot(BPR, I, 'r', 'LineWidth',3)
ylim([100 900])
grid on
ylabel('I [m/s]', "Rotation", 0, 'FontSize',20)
xlabel('BPR [-]', 'FontSize',20)