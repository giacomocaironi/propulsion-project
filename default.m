Z = 10700; % m
M = 0.85; % -

Tmax = 1400; % K
BPR = 10; % -
beta_c = 20; % -
beta_f = 1.4; % -
eff_d = 0.95; % -
eff_th_c = 0.87; % -
eff_mc_c = 0.99; % -
eff_th_t = 0.92; % -
eff_mc_t = 0.99; % -
eff_comb = 0.98; % -
eff_pn_comb = 0.97; % -
eff_n = 0.97; % -
Qf = 43 * 10^6; % J/kg
