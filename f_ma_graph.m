clear
clc
close all

default % richiama i dati

BPR_1 = 0:0.1:14; % range di BPR per costruire il grafico
TSFC_1 = []; % dichiarazione dei vettori
I_1 = [];
beta_c_1 = 10; % primo valore di rapporto di compressione
for bpr = BPR_1
    [i_1, tsfc_1, ~, ~, ~] = perfomance_turbofan(Z, M, Tmax, bpr, beta_f, beta_c_1, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    TSFC_1 = [TSFC_1 tsfc_1];
    I_1 = [I_1 i_1];
end

BPR_2 = 0:0.1:13.5; % scegliamo un range minore per via del più alto rapporto di compressione
TSFC_2 = [];
I_2 = [];
beta_c_2 = 20; % secondo valore di rapporto di compressione
for bpr = BPR_2
    [i_2, tsfc_2, ~, ~, ~] = perfomance_turbofan(Z, M, Tmax, bpr, beta_f, beta_c_2, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    TSFC_2 = [TSFC_2 tsfc_2];
    I_2 = [I_2 i_2];
end

BPR_3 = 0:0.1:12;
TSFC_3 = [];
I_3 = [];
beta_c_3 = 30; % terzo valore di rapporto di compressione
for bpr = BPR_3
    [i_3, tsfc_3, ~, ~, ~] = perfomance_turbofan(Z, M, Tmax, bpr, beta_f, beta_c_3, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    TSFC_3 = [TSFC_3 tsfc_3];
    I_3 = [I_3 i_3];
end

Ia_1 = I_1 .* (1+BPR_1); % spinta specifica definita come la spinta moltiplicata per (1+BPR)
Ia_2 = I_2 .* (1+BPR_2);
Ia_3 = I_3 .* (1+BPR_3);
figure("Name", "F/ma1") % Forza per unita di massa nel flusso principale
plot(BPR_1, Ia_1, 'r', 'LineWidth',3) 
grid on
hold on
plot(BPR_2, Ia_2, 'k', 'LineWidth',3)
plot(BPR_3, Ia_3, 'b', 'LineWidth',3)
ylabel('Ia [m/s]', "Rotation", 0, 'FontSize',20)
xlabel('BPR [-]', 'FontSize',20)
legend('\beta_c 10', '\beta_c 20', '\beta_c 30', 'FontSize',40)





