clear
clc

dati_decollo
[I, TSFC, nth, np, no] = perfomance_turbofan(Z, M, Tmax, BPR, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
[I, TSFC, nth, np, no]

dati_crociera
[I, TSFC, nth, np, no] = perfomance_turbofan(Z, M, Tmax, BPR, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
[I, TSFC, nth, np, no]