clear
clc
close all

default

f_1 = 0.0169; % range di BPR per costruire il grafico
I_1 = [];
TSFC_1 = [];
NTH_1 = [];
NP_1 = []; % dichiarazione dei vettori
NO_1 = [];
M_1 = 0.2:0.01:0.9; % primo valore di rapporto di compressione
for m = M_1
    [i_1, tsfc_1, nth_1, np_1, no_1] = perfomance_turbofan_2(Z, m, f_1, BPR, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    I_1 = [I_1 i_1];
    TSFC_1 = [TSFC_1 tsfc_1];
    NTH_1 = [NTH_1 nth_1];
    NP_1 = [NP_1 np_1];
    NO_1 = [NO_1 no_1];
end

f_2 = 0.017; 
I_2 = [];
TSFC_2 = [];
NTH_2 = [];
NP_2 = []; % dichiarazione dei vettori
NO_2 = [];
M_2 = 0.2:0.01:0.9; % primo valore di rapporto di compressione
for m = M_2
    [i_2, tsfc_2, nth_2, np_2, no_2] = perfomance_turbofan_2(Z, m, f_2, BPR, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    I_2 = [I_2 i_2];
    TSFC_2 = [TSFC_2 tsfc_2];
    NTH_2 = [NTH_2 nth_2];
    NP_2 = [NP_2 np_2];
    NO_2 = [NO_2 no_2];
end

f_3 = 0.018; 
I_3 = [];
TSFC_3 = [];
NTH_3 = [];
NP_3 = []; % dichiarazione dei vettori
NO_3 = [];
M_3 = 0.2:0.01:0.9; % primo valore di rapporto di compressione
for m = M_3
    [i_3, tsfc_3, nth_3, np_3, no_3] = perfomance_turbofan_2(Z, m, f_3, BPR, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    I_3 = [I_3 i_3];
    TSFC_3 = [TSFC_3 tsfc_3];
    NTH_3 = [NTH_3 nth_3];
    NP_3 = [NP_3 np_3];
    NO_3 = [NO_3 no_3];
end

f_4 = 0.019; 
I_4 = [];
TSFC_4 = [];
NTH_4 = [];
NP_4 = []; % dichiarazione dei vettori
NO_4 = [];
M_4 = 0.2:0.01:0.9; % primo valore di rapporto di compressione
for m = M_4
    [i_4, tsfc_4, nth_4, np_4, no_4] = perfomance_turbofan_2(Z, m, f_4, BPR, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    I_4 = [I_4 i_4];
    TSFC_4 = [TSFC_4 tsfc_4];
    NTH_4 = [NTH_4 nth_4];
    NP_4 = [NP_4 np_4];
    NO_4 = [NO_4 no_4];
end

f_5 = 0.0198; % range di BPR per costruire il grafico
I_5 = [];
TSFC_5 = [];
NTH_5 = [];
NP_5 = []; % dichiarazione dei vettori
NO_5 = [];
M_5 = 0.2:0.01:0.9; % primo valore di rapporto di compressione
for m = M_5
    [i_5, tsfc_5, nth_5, np_5, no_5] = perfomance_turbofan_2(Z, m, f_5, BPR, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    I_5 = [I_5 i_5];
    TSFC_5 = [TSFC_5 tsfc_5];
    NTH_5 = [NTH_5 nth_5];
    NP_5 = [NP_5 np_5];
    NO_5 = [NO_5 no_5];
end

grid on
hold on
plot(M_1, NTH_1, "color", '#7E2F8E', 'LineWidth',2) 
plot(M_3, NTH_3,"color", '#77AC30', 'LineStyle', '-.', 'LineWidth',1)
plot(M_4, NTH_4,"color", 'k','LineStyle','--', 'LineWidth',1)
plot(M_5, NTH_5,"color", '#0072BD', 'LineWidth',2)
plot(M_1, NP_1,"color", '#7E2F8E', 'LineWidth',2) 
plot(M_3, NP_3,"color", '#77AC30', 'LineStyle', '-.', 'LineWidth',1)
plot(M_4, NP_4,"color", 'k','LineStyle','--', 'LineWidth',1)
plot(M_5, NP_5,"color", '#0072BD', 'LineWidth',2)
plot(M_1, NO_1,"color", '#7E2F8E', 'LineWidth',2) 
plot(M_3, NO_3,"color", '#77AC30', 'LineStyle', '-.', 'LineWidth',1)
plot(M_4, NO_4,"color", 'k','LineStyle','--', 'LineWidth',1)
plot(M_5, NO_5,"color", '#0072BD', 'LineWidth',2)
ylabel('\eta [-]', "Rotation", 0, 'FontSize',20)
xlabel('M [-]', 'FontSize',20)
legend('f_c_r_u_i_s_e', 'f 0.018', 'f 0.019',  'f_m_a_x','FontSize',30)
ylim([0, 1])


figure
grid on
hold on
plot(M_1, I_1, "color", '#7E2F8E', 'LineWidth',2) 
plot(M_3, I_3,"color", '#77AC30', 'LineStyle', '-.', 'LineWidth',1)
plot(M_4, I_4,"color", 'k','LineStyle','--', 'LineWidth',1)
plot(M_5, I_5,"color", '#0072BD', 'LineWidth',2)
ylabel('I [m/s]', "Rotation", 0, 'FontSize',20)
xlabel('M [-]', 'FontSize',20)
legend('f_c_r_u_i_s_e', 'f 0.018', 'f 0.019',  'f_m_a_x','FontSize',30)