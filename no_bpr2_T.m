clear
clc
close all

default

T_1 = 1200; % range di BPR per costruire il grafico
NO_1 = []; % dichiarazione dei vettori
BPR_1 = 0:0.01:9.3; 
for bpr = BPR_1
    [~, ~, ~, ~, no_1] = perfomance_turbofan(Z, M, T_1, bpr, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NO_1 = [NO_1 no_1];
end

T_2= 1400; % range di BPR per costruire il grafico
NO_2 = []; % dichiarazione dei vettori
BPR_2 = 0:0.01:13.7; 
for bpr = BPR_2
    [~, ~, ~, ~, no_2] = perfomance_turbofan(Z, M, T_2, bpr, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NO_2 = [NO_2 no_2];
end

T_3= 1600; % range di BPR per costruire il grafico
eff_d_3 = 0.85;
NO_3 = []; % dichiarazione dei vettori
BPR_3 = 0:0.01:18.1; 
for bpr = BPR_3
    [~, ~, ~, ~, no_3] = perfomance_turbofan(Z, M, T_3, bpr, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NO_3 = [NO_3 no_3];
end



plot(BPR_1, NO_1, 'r', 'LineWidth',3) 
grid on
hold on
plot(BPR_2, NO_2, 'k', 'LineWidth',3)
plot(BPR_3, NO_3, 'b', 'LineWidth',3)
ylabel('\eta_n_o [-]', "Rotation", 0, 'FontSize',20)
xlabel('BPR [-]', 'FontSize',20)
legend('T_m_a_x 1200', 'T_m_a_x 1400', 'T_m_a_x 1600', 'FontSize',40)
ylim([0, 0.5])
