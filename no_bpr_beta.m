clear
clc
close all

default

beta_c_1 = 10; 
NO_1 = []; 
BPR_1 = 0:0.01:14.5; 
for bpr = BPR_1
    [~, ~, ~, ~, no_1] = perfomance_turbofan(Z, M, Tmax, bpr, beta_f, beta_c_1, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NO_1 = [NO_1 no_1];
end

beta_c_2 = 20; 
NO_2 = []; % dichiarazione dei vettori
BPR_2 = 0:0.01:13.5; 
for bpr = BPR_2
    [~, ~, ~, ~, no_2] = perfomance_turbofan(Z, M, Tmax, bpr, beta_f, beta_c_2, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NO_2 = [NO_2 no_2];
end

beta_c_3 = 30; 
NO_3 = []; % dichiarazione dei vettori
BPR_3 = 0:0.01:12; 
for bpr = BPR_3
    [~, ~, ~, ~, no_3] = perfomance_turbofan(Z, M, Tmax, bpr, beta_f, beta_c_3, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NO_3 = [NO_3 no_3];
end


plot(BPR_1, NO_1, 'r', 'LineWidth',3) 
grid on
hold on
plot(BPR_2, NO_2, 'k', 'LineWidth',3)
plot(BPR_3, NO_3, 'b', 'LineWidth',3)
ylabel('\eta_n_o [-]', "Rotation", 0, 'FontSize',20)
xlabel('BPR [-]', 'FontSize',20)
legend('\beta_c 10', '\beta_c 20', '\beta_c 30', 'FontSize',40)

