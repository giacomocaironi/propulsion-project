clear
clc
close all

default

BPR_1 = 4; % range di BPR per costruire il grafico
NP_1 = []; % dichiarazione dei vettori
M_1 = 0:0.01:2; % primo valore di rapporto di compressione
for m = M_1
    [~, ~, ~, np_1, ~] = perfomance_turbofan(Z, m, Tmax, BPR_1, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NP_1 = [NP_1 np_1];
end

BPR_2 = 8; % range di BPR per costruire il grafico
NP_2 = []; % dichiarazione dei vettori
M_2 = 0:0.01:1.6; % primo valore di rapporto di compressione
for m = M_2
    [~, ~, ~, np_2, ~] = perfomance_turbofan(Z, m, Tmax, BPR_2, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NP_2 = [NP_2 np_2];
end

BPR_3 = 12; % range di BPR per costruire il grafico
NP_3 = []; % dichiarazione dei vettori
M_3 = 0:0.01:1.15; % primo valore di rapporto di compressione
for m = M_3
    [~, ~, ~, np_3, ~] = perfomance_turbofan(Z, m, Tmax, BPR_3, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NP_3 = [NP_3 np_3];
end


plot(M_1, NP_1, 'r', 'LineWidth',3) 
grid on
hold on
plot(M_2, NP_2, 'k', 'LineWidth',3)
plot(M_3, NP_3, 'b', 'LineWidth',3)
ylabel('\eta_n_p [-]', "Rotation", 0, 'FontSize',20)
xlabel('M [-]', 'FontSize',20)
legend('BPR 4', 'BPR 8', 'BPR 12', 'FontSize',40)
ylim([0, 1])
