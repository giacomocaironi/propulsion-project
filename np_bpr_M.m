clear
clc
close all

default

M_1 = 0.8; % range di BPR per costruire il grafico
NP_1 = []; % dichiarazione dei vettori
BPR_1 = 0:0.01:13.9; 
for bpr = BPR_1
    [~, ~, ~, np_1, ~] = perfomance_turbofan(Z, M_1, Tmax, bpr, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NP_1 = [NP_1 np_1];
end

M_2 = 1.5; % range di BPR per costruire il grafico
eff_d_2 = 0.85;
NP_2 = []; % dichiarazione dei vettori
BPR_2 = 0:0.01:9.4; 
for bpr = BPR_2
    [~, ~, ~, np_2, ~] = perfomance_turbofan(Z, M_2, Tmax, bpr, beta_f, beta_c, eff_d_2, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NP_2 = [NP_2 np_2];
end



plot(BPR_1, NP_1, 'r', 'LineWidth',3) 
grid on
hold on
plot(BPR_2, NP_2, 'k', 'LineWidth',3)
ylabel('\eta_n_p [-]', "Rotation", 0, 'FontSize',20)
xlabel('BPR [-]', 'FontSize',20)
legend('Mach 0.8', 'Mach 1.5', 'FontSize',40)
ylim([0, 1])
