clear
clc
close all

default

M_1 = 0.0:0.01:1.5;
NTH_1 = [];
beta_c_1 = 10;
for m = M_1
    [~, ~, nth_1, ~, ~] = perfomance_turbofan(Z, m, Tmax, BPR, beta_f, beta_c_1, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NTH_1 = [NTH_1 nth_1];
end

M_2 = 0.0:0.01:1.5;
NTH_2 = [];
beta_c_2 = 20;
for m = M_2
    [~, ~, nth_2, ~, ~] = perfomance_turbofan(Z, m, Tmax, BPR, beta_f, beta_c_2, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NTH_2 = [NTH_2 nth_2];
end

M_3 = 0.0:0.01:1.5;
NTH_3 = [];
beta_c_3 = 30;
for m = M_1
    [~, ~, nth_3, ~, ~] = perfomance_turbofan(Z, m, Tmax, BPR, beta_f, beta_c_3, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    NTH_3 = [NTH_3 nth_3];
end

% Forza per unita di massa nel flusso principale
plot(M_1, NTH_1, 'r', 'LineWidth',3) 
grid on
hold on
plot(M_2, NTH_2, 'k', 'LineWidth',3)
plot(M_3, NTH_3, 'b', 'LineWidth',3)
ylabel('\eta_n_h [-]', "Rotation", 0, 'FontSize',20)
xlabel('M [-]', 'FontSize',20)
legend('\beta_c 10', '\beta_c 20', '\beta_c 30', 'FontSize',40)

