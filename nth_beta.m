clear
clc
close all

default

BPR_1 = 4; % range di BPR per costruire il grafico
NTH_1 = []; % dichiarazione dei vettori
beta_c_1 = 0:0.01:30; % primo valore di rapporto di compressione
for betac = beta_c_1
   [~, ~, nth_1, ~, ~] = perfomance_turbofan(Z, M, Tmax, BPR_1, beta_f, betac, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
   NTH_1 = [NTH_1 nth_1];
end

BPR_2 = 8; % range di BPR per costruire il grafico
NTH_2 = []; % dichiarazione dei vettori
beta_c_2 = 0:0.01:30; % primo valore di rapporto di compressione
for betac = beta_c_2
   [~, ~, nth_2, ~, ~] = perfomance_turbofan(Z, M, Tmax, BPR_2, beta_f, betac, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
   NTH_2 = [NTH_2 nth_2];
end

BPR_3 = 12; % range di BPR per costruire il grafico
NTH_3 = []; % dichiarazione dei vettori
beta_c_3 = 0:0.01:30; % primo valore di rapporto di compressione
for betac = beta_c_3
   [~, ~, nth_3, ~, ~] = perfomance_turbofan(Z, M, Tmax, BPR_3, beta_f, betac, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
   NTH_3 = [NTH_3 nth_3];
end


plot(beta_c_1, NTH_1, 'r', 'LineWidth',3) 
grid on
hold on
plot(beta_c_2, NTH_2, 'k', 'LineWidth',3)
plot(beta_c_3, NTH_3, 'b', 'LineWidth',3)
ylabel('\eta_n_h [-]', "Rotation", 0, 'FontSize',20)
xlabel('\beta_c [-]', 'FontSize',20)
legend('BPR 4', 'BPR 8', 'BPR 12', 'FontSize',40)
ylim([0, 1])
