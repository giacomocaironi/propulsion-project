function [I, TSFC, nth, np, no] = perfomance_turbofan(Z, M, Tmax, BPR, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf)
% Parametri di performance del turbofan a flussi separati
%
% [I, TSFC, nth, np, no] = perfomance_turbofan(Z, M, Tmax, BPR, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf)
%
% Dati in input:
% -------------------------------------------------------------------------
% Z           [1x1]   altitudine                                    [m]
% M           [1x1]   numero di Mach                                [-]
% Tmax        [1x1]   temperqatura massima                          [K]
% BPR         [1x1]   rapporto di bypass                            [-]
% beta_f      [1x1]   rapporto di compressione del fan              [-]
% beta_c      [1x1]   rapporto di compressione del compressore      [-]
% eff_d       [1x1]   efficienza del diffusore                      [-]
% eff_th_c    [1x1]   efficienza del compressore                    [-]
% eff_th_t    [1x1]   efficienza della turbina                      [-]
% eff_n       [1x1]   efficienza ugello                             [-]
% eff_comb    [1x1]   efficienza del combustore                     [-]
% eff_pn_comb [1x1]   efficienza pneumatica del combustore          [-]
% eff_mc_c    [1x1]   efficienza meccanica del compressore          [-]
% eff_mc_t    [1x1]   efficienza meccanica della turbina            [-]
% Qf          [1x1]   potere calorifico del combustibile            [J/kg]
%
% -------------------------------------------------------------------------
% Dati in output:
% I           [1x1]   impulso                                       [m/s]
% TSFC        [1x1]   consumo specifico                             [kg*h/N]
% nth         [1x1]   rendimento termodinamico                      [-]
% np          [1x1]   rendimento propulsivo                         [-]
% no          [1x1]   rendimento globale                            [-]

a = -6.5/1000;
R = 287.1;
g = 9.81;
gamma = 1.4;
cp = 7 / 2 * R;

Ta = 288.15 + a * Z;
Pa = 101325 * (Ta / 288.15) ^ (-g / (R * a));
V0 = (gamma * R * Ta)^0.5 * M;

T1 = Ta;
P1 = Pa;

T2 = T1 * (1 + (gamma-1)/2*M^2);
%P2 = P1 * (T2/Ta)^(gamma/(gamma-1)) * eff_d;
P2 = P1 * (1 + (gamma-1)/2*M^2*eff_d)^(gamma/(gamma-1));

T21 = T2 * (1 + (beta_f^((gamma - 1)/gamma) - 1) / eff_th_c);
P21 = beta_f * P2;
Lf = cp * (T21 - T2) / eff_mc_c * (1 + BPR);

T3 = T21 * (1 + (beta_c^((gamma - 1)/gamma) - 1) / eff_th_c);
P3 = beta_c * P21;
Lc = cp * (T3 - T21) / eff_mc_c;

T4 = Tmax;
P4 = P3 * eff_pn_comb;
f = cp * (T4 - T3) / (eff_comb * Qf - cp * T4);

Lht = Lc / eff_mc_t;
T41 = T4 - Lht / ((1+f) * cp);
P41 = P4 * (1 - (1 - T41 / T4)/(eff_th_t))^(gamma/(gamma-1));

Llt = Lf / eff_mc_t;
T5 = T41 - Llt / ((1+f) * cp);
P5 = P41 * (1 - (1 - T5 / T41)/(eff_th_t))^(gamma/(gamma-1));

u9 = (2 * cp * T5 * eff_n * (1 - (Pa/P5)^((gamma-1)/gamma)))^0.5;
u19 = (2 * cp * T21 * eff_n * (1 - (Pa/P21)^((gamma-1)/gamma)))^0.5;

I = ((1+f)*u9 + BPR * u19) / (1+BPR) - V0;
TSFC = f/I/(1+BPR)*3600;
nth = ((1+f)*u9^2-(1-f)*V0^2+BPR*(u19^2-V0^2))/(2*f*Qf);
np = 2*I*V0*(1+BPR)/((1+f)*u9^2-(1-f)*V0^2+BPR*(u19^2-V0^2));
no = nth*np;

f

end

