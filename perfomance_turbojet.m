function [I, TSFC, nth, np, no] = perfomance_turbojet(Z, M, Tmax, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf)

a = -6.5/1000;
R = 287.1;
g = 9.81;
gamma = 1.4;
cp = 7 / 2 * R;

Ta = 288.15 + a * Z;
Pa = 101325 * (Ta / 288.15) ^ (-g / (R * a));
V0 = (gamma * R * Ta)^0.5 * M;

T1 = Ta;
P1 = Pa;

T2 = T1 * (1 + (gamma-1)/2*M^2);
%P2 = P1 * (T2/Ta)^(gamma/(gamma-1)) * eff_d;
P2 = P1 * (1 + (gamma-1)/2*M^2*eff_d)^(gamma/(gamma-1));

T3 = T2 * (1 + (beta_c^((gamma - 1)/gamma) - 1) / eff_th_c);
P3 = beta_c * P2;
Lc = cp * (T3 - T2) / eff_mc_c;

T4 = Tmax;
P4 = P3 * eff_pn_comb;
f = cp * (T4 - T3) / (eff_comb * Qf - cp * T4);

Lt = Lc / eff_mc_t;
T5 = T4 - Lt / ((1+f) * cp);
P5 = P4 * (1 - (1 - T5 / T4)/(eff_th_t))^(gamma/(gamma-1));

P9 = Pa;
u9 = (2 * cp * T5 * eff_n * (1 - (P9/P5)^((gamma-1)/gamma)))^0.5;

I = (1+f)*u9 - V0;
TSFC = f/I * 3600;
nth = ((1+f)*ue^2-V0^2)/(2*f*Qf);
np = 2*I*V0/((1+f)*ue^2-V0^2);
no = nth*np;

end

