clear
clc
close all

default

BPR = 0:0.1:11.5;
TSFC = [];
I = [];
for bpr = BPR
    [i, tsfc, ~, ~, ~] = perfomance_turbofan(Z, M, Tmax, bpr, beta_f, beta_c, eff_d, eff_th_c, eff_th_t, eff_n, eff_comb, eff_pn_comb, eff_mc_c, eff_mc_t, Qf);
    TSFC = [TSFC tsfc];
    I = [I i];
end

figure("Name", "TSFC")
plot(BPR, TSFC)
figure("Name", "I")
plot(BPR, I)
ylim([100 900])
Ia = I .* (1+BPR);
figure("Name", "F/ma")
plot(BPR, Ia)